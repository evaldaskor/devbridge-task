// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var webserver = require('gulp-webserver');
var addsrc = require('gulp-add-src');
var livereload = require('gulp-livereload');

// Webserver Task
gulp.task('webserver', function() {
  gulp.src('dist')
    .pipe(webserver({
      livereload: true,
      open: true
    }));
});

// Lint Task
gulp.task('lint', function() {
    return gulp.src('js/modules/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass

var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');

gulp.task('sass', function() {
    return gulp.src('scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([
            autoprefixer({browsers: ['last 5 version']})
        ]))
        .pipe(gulp.dest('dist/css'));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src('js/modules/*.js')
        .pipe(addsrc.prepend('js/main-before.js'))
        .pipe(addsrc.prepend('js/vendors.js'))
        .pipe(addsrc.append('js/main-after.js'))
        .pipe(concat('all.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('scripts-vendors', function() {
    return gulp.src('js/vendors/*.js')
        .pipe(concat('vendors.js'))
        .pipe(gulp.dest('js'))
        .pipe(livereload());
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('js/modules/*.js', ['lint', 'scripts']);
    gulp.watch('js/vendors/*.js', ['scripts-vendors']);
    gulp.watch('scss/**/*.scss', ['sass']);
});


// Default Task
gulp.task('default', ['webserver', 'lint', 'sass', 'scripts', 'scripts-vendors', 'watch']);
(function(){
	 var toggle = document.querySelector('.header__nav-toggle');
	 var nav = document.querySelector('.header__nav');
	 var navItem = document.querySelectorAll('.has-children > a');

		function mainNavToggle(e) {
			e.preventDefault();
			 toggle.classList.toggle('open');
			 nav.classList.toggle('open');
		}

	for (var x = 0; x < navItem.length; x++) {
			navItem[x].onclick = function(event) {
				event.preventDefault();
				this.parentElement.classList.toggle('open');
			};
	}

	toggle.addEventListener('click',  mainNavToggle);
})();

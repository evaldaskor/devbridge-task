(function(){

	var fields = [
		{name: "name", required: true},
		{name: "lastname", required: true}
	];

	var form = document.getElementById("form");
	var msg = document.getElementById("msg");

	form.addEventListener("submit", function(e) {

		e.preventDefault();
		var msgStr = "";
		var noError= "0";

		for (var i = 0; i < fields.length; i++) {
			if (fields[i].required === true && form[fields[i].name].value === "") {
				form[fields[i].name].className = "error";
				msgStr += "Please enter your " + fields[i].name + ". ";
			} else {
				form[fields[i].name].className = "";
				noError++;
			}
		}

		if(noError == fields.length){
			msgStr = "Your message was sent successfully";
		}

		msg.innerHTML = msgStr;

	});

})();
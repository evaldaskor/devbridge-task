
window.onload = function() {
(function(){

	var fields = [
		{name: "name", required: true},
		{name: "lastname", required: true}
	];

	var form = document.getElementById("form");
	var msg = document.getElementById("msg");

	form.addEventListener("submit", function(e) {

		e.preventDefault();
		var msgStr = "";
		var noError= "0";

		for (var i = 0; i < fields.length; i++) {
			if (fields[i].required === true && form[fields[i].name].value === "") {
				form[fields[i].name].className = "error";
				msgStr += "Please enter your " + fields[i].name + ". ";
			} else {
				form[fields[i].name].className = "";
				noError++;
			}
		}

		if(noError == fields.length){
			msgStr = "Your message was sent successfully";
		}

		msg.innerHTML = msgStr;

	});

})();
(function(){
	 var toggle = document.querySelector('.header__nav-toggle');
	 var nav = document.querySelector('.header__nav');
	 var navItem = document.querySelectorAll('.has-children > a');

		function mainNavToggle(e) {
			e.preventDefault();
			 toggle.classList.toggle('open');
			 nav.classList.toggle('open');
		}

	for (var x = 0; x < navItem.length; x++) {
			navItem[x].onclick = function(event) {
				event.preventDefault();
				this.parentElement.classList.toggle('open');
			};
	}

	toggle.addEventListener('click',  mainNavToggle);
})();

}